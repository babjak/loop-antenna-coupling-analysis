mur = 100;
f = 10 * 1e6; % [Hz]

N1 = 3 * 200;
d1 = 0.005; % [m] radius
I1_0 = 1; % [A]

N2 = 3 * 200;
d2 = 0.005; % [m] radius
Re = 9; % [ohm]

G_dB = 30; % [dB]
G = 10^(G_dB/10);


%---------------------------
mu0 = 4 * pi() * 1e-7; % [H/m]
epsilon0 = 8.85 * 1e-12; % [F/m]
c = 299792458; % [m/s]

T = 1/f;
lambda = T*c; 
beta = (2 * pi()) / lambda;

omega = 2 * pi() * f;

mu = mu0 * mur;
m = I1_0 * pi() * d1^2;
eta = sqrt(mu / epsilon0);


%---------------------------
part1 = (N2 * pi() * d2^2)/Re;
I2 = [];
theta1_cum = [];
theta2_cum = [];

R_max = 10;
R_min = 1;
R_step = 1;

theta_max = pi();
theta_min = 0;
theta_step = pi()/18;

phi = 0;
for R = R_min:R_step:R_max
    const = (1i * omega * mu * beta^2 * m) / (4 * pi() * eta) * exp(-1i * beta * R);

    I_max = 0;
    theta1_max = 0;
    theta2_max = 0;
    
    I_plot = [];    
    column = 1;
    
    H_plot = [];
    
    for theta1 = theta_min:theta_step:theta_max
        %spherical coordinate system with unit vectors at point phi = 0, R, theta1
        % R
        H(1) = -2 * const * cos(theta1) * (1/(1i * beta * R)^2 + 1/(1i * beta * R)^3);
        H_plot(end + 1, 1) = abs(H(1));

        % theta
        H(2) = -const * sin(theta1) * (1/(1i * beta * R) + 1/(1i * beta * R)^2 + 1/(1i * beta * R)^3);
        H_plot(end, 2) = abs(H(2));
        
        % phi
        H(3) = 0;
        
        transf_matrix = [sin(theta1)*cos(phi) sin(theta1)*sin(phi) cos(theta1); cos(theta1)*cos(phi) cos(theta1)*sin(phi) -sin(theta1); -sin(phi) cos(phi) 0];
        
        %cartesian coordinates
        H_cart = H * transf_matrix;
        
        B = H_cart * mu;

        row = 1;

        for theta2 = theta_min:theta_step:theta_max
            %cartesian coordinate system
            %x y z
            n = [sin(theta2) 0 cos(theta2)];
            
            B_surf = dot(B,n);            
            B_derivate = B_surf*1i*omega;
            
            I = abs(part1 * B_derivate);
            I_plot(row, column) = I;
            if I > I_max
                I_max = I;
                theta1_max = theta1;
                theta2_max = theta2;
            end
            
            row = row + 1;
        end
        
        column = column + 1;
    end
    
%     figure;
%     hold on;
%     plot((theta_min:theta_step:theta_max), I_plot);
%     plot(theta2_max, I_max, 'ro');
%     title(['theta1 = ' num2str(theta1_max) '; theta2 = ' num2str(theta2_max)]); 
%     hold off;

%     figure;
%     hold on;
%     plot((theta_min:theta_step:theta_max), H_plot);
%     HR_max = max(H_plot(:,1));
%     Htheta_max = max(H_plot(:,2));
%     plot([theta_min theta_max], [HR_max HR_max]);
%     plot([theta_min theta_max], [Htheta_max Htheta_max]);
%     hold off;
    
    I2(end + 1) = I_max;
    theta1_cum(end + 1) = theta1_max;
    theta2_cum(end + 1) = theta2_max;
end

figure;
plot((R_min:R_step:R_max), I2);

figure;
plot([R_min:R_step:R_max], theta1_cum, [R_min:R_step:R_max], theta2_cum);