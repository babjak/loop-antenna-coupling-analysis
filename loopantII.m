mu0 = 4 * pi() * 1e-7;
mur = 100;
f = 1e7; % [Hz]

N1 = 200;
d1 = 0.005; % [m]
I1_0 = 1; % [A]

N2 = 200;
d2 = 0.005; % [m]
Re = 9; % [ohm]

G_dB = 30; % [dB]
G = 10^(G_dB/10);

part1 = (N2 * pi() * d2^2)/Re;
part3 = 2 * pi() * f * I1_0 * G;

I2 = [];
for R = 1:10
    part2 = (2 * mu0 * mur * N1 * d1^2)/(4 * R^3);
    I2(end + 1) = part1 * part2 * part3;
end
R = 1:10;

figure;
plot(R, I2);